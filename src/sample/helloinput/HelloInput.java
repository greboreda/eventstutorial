package sample.helloinput;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import sample.ViewLoader;
import sample.events.CustomEvent;

import java.util.logging.Logger;


public class HelloInput extends AnchorPane {

	private static final Logger logger = Logger.getLogger(HelloInput.class.getSimpleName());

	@FXML
	private TextField input;
	@FXML
	private Button byEvent;
	@FXML
	private Button byLambda;

	private EventHandler<CustomEvent> handler;
	private Node nodeTiFire;

	public HelloInput() {
		ViewLoader.load(this, "helloinput/helloinput.fxml");
	}

	@FXML
	public void initialize() {
		byEvent.setOnMouseClicked(this::onClickByEventButton);
		byLambda.setOnMouseClicked(this::onClickByLambdaButton);
	}

	private void onClickByEventButton(MouseEvent mouseEvent) {
		logger.info("click on event button! let's fire event!");
		final String text = input.getText();
		final CustomEvent customEvent = new CustomEvent(text);
		nodeTiFire.fireEvent(customEvent);
	}

	private void onClickByLambdaButton(MouseEvent mouseEvent) {
		logger.info("click on lambda button! let's handle handler!");
		final String text = input.getText();
		final CustomEvent customEvent = new CustomEvent(text);
		handler.handle(customEvent);
	}

	public void setOnHasData(EventHandler<CustomEvent> handler) {
		this.handler = handler;
	}

	public void setNodeTiFire(Node nodeTiFire) {
		this.nodeTiFire = nodeTiFire;
	}
}
