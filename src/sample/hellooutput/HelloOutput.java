package sample.hellooutput;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import sample.ViewLoader;
import sample.events.CustomEvent;

import java.util.logging.Logger;

import static sample.events.CustomEvent.CUSTOM_EVENT;


public class HelloOutput extends AnchorPane {

	private static final Logger logger = Logger.getLogger(HelloOutput.class.getSimpleName());

	@FXML
	private TextArea output;

	public HelloOutput() {
		ViewLoader.load(this,"hellooutput/hellooutput.fxml");
	}

	@FXML
	public void initialize() {
		addEventHandler(CUSTOM_EVENT, this::onCustomEvent);
	}

	private void onCustomEvent(CustomEvent customEvent) {
		logger.info("catched event!");
		final String data = customEvent.data;
		output.setText(data);
	}

	public void refresh(String data) {
		logger.info("refresh!");
		output.setText(data);
	}

}
