package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.URL;

public class ViewLoader {

	public static Parent load(Object object, String fxml) {
		final URL resource = ViewLoader.class.getResource(fxml);
		final FXMLLoader loader = new FXMLLoader(resource);
		loader.setController(object);
		loader.setRoot(object);
		try {
			return loader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

}
