package sample.main;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import sample.ViewLoader;
import sample.helloinput.HelloInput;
import sample.hellooutput.HelloOutput;

import java.util.logging.Logger;

public class MainController extends HBox {

	private static final Logger logger = Logger.getLogger(MainController.class.getSimpleName());

	public final Parent parent;

	@FXML
	private HelloInput helloInput;
	@FXML
	private HelloOutput helloOutput;

	public MainController() {
		this.parent = ViewLoader.load(this, "main/main.fxml");
	}

	@FXML
	public void initialize() {

		helloInput.setNodeTiFire(helloOutput);

		helloInput.setOnHasData(customEvent -> {
			logger.info("input handled handler! let's refresh output!");
			helloOutput.refresh(customEvent.data);
		});
	}

}
