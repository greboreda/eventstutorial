package sample.events;

import javafx.event.Event;
import javafx.event.EventType;

public class CustomEvent extends Event {

	public static final EventType<CustomEvent> CUSTOM_EVENT = new EventType<>("CUSTOM_EVENT");
	public final String data;

	public CustomEvent(String data) {
		super(CUSTOM_EVENT);
		this.data = data;
	}

}
